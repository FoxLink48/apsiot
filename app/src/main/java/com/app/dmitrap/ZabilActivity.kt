package com.app.dmitrap

import android.app.AlertDialog
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.net.http.SslError
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Message
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.Button
import android.widget.ImageView
import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*


const val SHARED_PREFS_URL2 = "SHARED URL"

class ZabilActivity : AppCompatActivity() {

    companion object {
        var firstUrl = ""
        var isFirst = true
        var isDomain = false
        var result_url:String? = null
    }
    lateinit var webView:WebView
    var editor: SharedPreferences.Editor? = null

    private var mUploadMessage: ValueCallback<Array<Uri>>? = null
    private var cameraImagePath: String? = null

    lateinit var button2:Button
    lateinit var imageView2:ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)
        Log.e("WEB_VIEW", "onCreate")
        val urlData = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .getString("URL_2_DATA", "")
        getCustomWebChromeClient()

        window.decorView.apply {
            // Hide both the navigation bar and the status bar.
            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
            // a general rule, you should design your app to hide the status bar whenever you
            // hide the navigation bar.

            systemUiVisibility =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        val integraciya =
            PreferenceManager.getDefaultSharedPreferences(this).getString("Integraciya", "")
        val clent_id =
            PreferenceManager.getDefaultSharedPreferences(this).getString("Cleand_id", "")
        val child_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Child_id", "")
        val email = PreferenceManager.getDefaultSharedPreferences(this).getString("email", "")
        val password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "")
        webView = WebView(this)
        setContentView(webView)

        webView.loadUrl("https://aspiot.ru/remind_password")

        webView.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(
                view: WebView?, handler: SslErrorHandler,
                error: SslError
            ) {
                val builder = AlertDialog.Builder(this@ZabilActivity)
                var message = "SSL Certificate error."
                when (error.primaryError) {
                    SslError.SSL_UNTRUSTED -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "SslError : The certificate authority is not trusted."
                        )
                        message = "The certificate authority is not trusted.";
                    }
                    SslError.SSL_EXPIRED -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "SslError : The certificate has expired."
                        )
                        message = "The certificate has expired.";
                    }
                    SslError.SSL_IDMISMATCH -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate Hostname mismatch."
                        )
                        message = "The certificate Hostname mismatch."
                    }
                    SslError.SSL_NOTYETVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate is not yet valid."
                        )
                        message = "The certificate is not yet valid."
                    }
                    SslError.SSL_DATE_INVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate date is not valid."
                        )
                        message = "The certificate date is not valid."
                    }
                    SslError.SSL_INVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate is not valid."
                        )
                        message = "The certificate is not valid."
                    }
                }
                message += " Do you want to continue anyway?"

                builder.setTitle("SSL Certificate Error")
                builder.setMessage(message)

                builder.setPositiveButton(
                    "continue"
                ) { dialog, which -> handler.proceed() }
                builder.setNegativeButton(
                    "cancel"
                ) { dialog, which -> handler.cancel() }
                val dialog = builder.create()
                dialog.show()
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.e("Success", "URL $url")

                Webview.firstUrl = PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity)
                    .getString("URL_FIRST", "")!!
                Log.e("Success", "URL First ${Webview.firstUrl}")

                if (url!!.contains(Webview.firstUrl)) {
                    if (Webview.isFirst)
                        PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
                            .putString("URL_FIRST", url!!.substringAfter("://")).apply()
                    if (url.contains("http")) {
                        Log.e("LOG_TAG", "shouldOverrideUrlLoading URL1: $url ")
                        view?.loadUrl(url)
                        Webview.isFirst = false
                    }
                } else {
                    val list = urlData!!.split(",")
                    var isTrue = false
                    for (item in list) {
                        if (url.contains(item)) {
                            isTrue = true
                            Webview.isDomain = true
                        }
                    }
                    if (!Webview.isDomain || isTrue) {
                        Log.e("LOG_TAG", "shouldOverrideUrlLoading URL1: $url ")
                        view?.loadUrl(url)

                    } else {
                        try {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            intent.data = Uri.parse(url)
                            startActivity(intent)
                        } catch (e: Exception) {
                            Log.e("TAG", "Error no such program")

                        }
                    }


                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)
            }
        }
        configureWebViewSettings()

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {

                Witi()



                if (!url.contains("http")) {
                    val intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK
                    )
                    startActivity(intent)
                    return true
                } else {
                    webView.loadUrl(url)
                    return true
                }
                return false;
            }
        }



        webView.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            //filename of downloading file
            var filename = URLUtil.guessFileName(url, contentDisposition, mimetype)

            //alertdialog builder created
            val builder = AlertDialog.Builder(this)
            //alertdialog title set
            builder.setTitle("Download")
            //alertdialog message set
            builder.setMessage("Do you want to save $filename")
            //if yes clicks,following code will executed
            builder.setPositiveButton("Yes") { dialog, which ->
                //DownloadManager request created based on url
                val request = DownloadManager.Request(Uri.parse(url))
                //get cookie
                val cookie = CookieManager.getInstance().getCookie(url)
                //add cookie to request
                request.addRequestHeader("Cookie", cookie)
                //add User-agent to request
                request.addRequestHeader("User-Agent", userAgent)
                //Files are scanned before downloading
                request.allowScanningByMediaScanner()
                //download notification is visible while downloading and after download completion
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                //DownloadManager Service created
                val downloadmanager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                //Files are downloaded to Download folder
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
                //download starts
                downloadmanager.enqueue(request)
            }
            builder.setNegativeButton("Cancel")
            { dialog, which ->
                //dialog cancels
                dialog.cancel()
            }
            //alertdialog created
            val dialog: AlertDialog = builder.create()
            //shows alertdialog
            dialog.show()
        }


    }

    fun Witi(){
        val url_save = webView.url.toString()
        if(url_save == "https://aspiot.ru/?logout" || url_save == "https://aspiot.ru/client_login?ajax=1" || url_save == "https://aspiot.ru/client_login" || url_save == "https://aspiot.ru/remind_password"){
            startActivity(Intent(this@ZabilActivity, MainActivity::class.java))
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
                .putBoolean(
                    IS_FIRST, false
                ).apply()
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
                .putBoolean(
                    IS_SECOND, false
                ).apply()
        }else{
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
                .putBoolean(
                    IS_FIRST, true
                ).apply()
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
                .putBoolean(
                    IS_SECOND, true
                ).apply()
        }
    }


    private fun configureWebViewSettings() {
        webView.settings.javaScriptEnabled = true
        webView.settings.setAppCacheEnabled(true)
        webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = false
        webView.settings.blockNetworkImage = false
        webView.settings.loadsImagesAutomatically = true
        webView.settings.useWideViewPort = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.allowFileAccess = true
        webView.settings.domStorageEnabled = true
        webView.settings.setSupportMultipleWindows(true)
        webView.settings.loadWithOverviewMode = true
        webView.settings.allowContentAccess = true
        webView.settings.setGeolocationEnabled(true)
        webView.webChromeClient = getCustomWebChromeClient()
    }

    private fun getCustomWebChromeClient() = object : WebChromeClient() {

        override fun onCreateWindow(
            view: WebView?,
            isDialog: Boolean,
            isUserGesture: Boolean,
            resultMsg: Message?
        ): Boolean {
            val result = view!!.hitTestResult
            val data = result.extra
            view.loadUrl(data!!)
            return false
        }
    }

    private fun createImageCaptureIntent(): Intent? {
        var captureImageIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (captureImageIntent?.resolveActivity(packageManager) != null) {
            var imageFile: File? = null

            try {
                imageFile = createImageFile()
                captureImageIntent.putExtra("CameraImagePath", cameraImagePath)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

            if (imageFile != null) {
                cameraImagePath = Webview.CAMERA_PHOTO_PATH_POSTFIX + imageFile.absolutePath
                captureImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile))
            } else {
                captureImageIntent = null
            }
        }

        return captureImageIntent
    }

    private fun createImageFile(): File? {
        val timeStamp = SimpleDateFormat.getDateInstance().format(Date())
        val imageFileName = Webview.PHOTO_NAME_POSTFIX + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, Webview.PHOTO_FORMAT, storageDir)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != Webview.REQUEST_SELECT_FILE || mUploadMessage == null) return

        var results: Array<Uri>? = null

        if (resultCode == RESULT_OK) {
            if (data == null) {
                if (cameraImagePath != null) results = arrayOf(Uri.parse(cameraImagePath))
            } else {
                val dataString = data.dataString
                if (dataString != null) results = arrayOf(Uri.parse(dataString))
            }
        }
        mUploadMessage?.onReceiveValue(results)
        mUploadMessage = null
    }

    override fun onStop() {
        super.onStop()
        val argument = PreferenceManager.getDefaultSharedPreferences(this).getInt("argumentzabil", 0)
        if(argument == 1){
            webView.loadUrl("https://aspiot.ru/remind_password")
        }else{
            setURL(webView.url)
        }
    }

    private fun setURL(url: String?) {
        val editor = PreferenceManager.getDefaultSharedPreferences(this).edit()
        editor.putString(SHARED_PREFS_URL, url)
        editor.apply()
    }

    private fun unHideToLog(str: String): String {
        var str = str
        str = str.replace("-", "")
        var result = ""
        var i = 0
        while (i < str.length) {
            val hex = str.substring(i + 1, i + 3)
            result += (hex.toInt(16) xor str[i].toString().toInt()).toChar()
            i += 3
        }
        Log.i("UNHIDE", "OK $result")
        Webview.result_url = result
        return  result
    }

    private fun doBack(): Boolean {
        if (webView.canGoBack()) {
            webView.goBack()
            return true
        }
        startActivity(Intent(this@ZabilActivity, MainActivity::class.java))
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
            .putBoolean(
                IS_FIRST, false
            ).apply()
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@ZabilActivity).edit()
            .putBoolean(
                IS_SECOND2, false
            ).apply()
        return false
    }

    override fun onBackPressed() {
        doBack()
    }


    override fun onDestroy() {
        super.onDestroy()
    }
}