package com.app.dmitrap

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.*
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.net.http.SslError
import android.os.*
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.app.dmitrap.*
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*


const val IS_URL = "IS_URL"

class Webview : AppCompatActivity() {



    companion object {
        private const val CAMERA_REQUEST_CODE = 113
        const val REQUEST_SELECT_FILE = 1
        private const val INTENT_FILE_TYPE = "image/*"
        const val CAMERA_PHOTO_PATH_POSTFIX = "file:"
        const val PHOTO_NAME_POSTFIX = "JPEG_"
        const val PHOTO_FORMAT = ".jpg"
        var firstUrl = ""
        var isFirst = true
        var isDomain = false
        var result_url:String? = null
    }
    lateinit var webView:WebView
    var editor: SharedPreferences.Editor? = null

    var token:String? = null
    var success:String? = null
    private var mUploadMessage: ValueCallback<Array<Uri>>? = null
    private var cameraImagePath: String? = null
    var argument = 0

    lateinit var context: Context

    val TAG = MainActivity::class.java.simpleName

    val FILECHOOSER_RESULTCODE = 1
    var mCapturedImageURI: Uri? = null

    // the same for Android 5.0 methods only

    lateinit var mFilePathCallback: ValueCallback<Array<Uri?>?>
    lateinit var mCameraPhotoPath: String
    var url_:String? = null
    @SuppressLint("StringFormatInvalid")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
//        checkInternetConnection()
        Log.e("WEB_VIEW", "onCreate")
        val urlData = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .getString("URL_2_DATA", "")
        getCustomWebChromeClient()

        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
            .putBoolean(
                IS_SECOND, true
            ).apply()
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
            .putBoolean(
                IS_FIRST, true
            ).apply()
        window.decorView.apply {
            // Hide both the navigation bar and the status bar.
            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
            // a general rule, you should design your app to hide the status bar whenever you
            // hide the navigation bar.

            systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }



        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
            .putBoolean(
                IS_URL, true
            ).apply()
        val integraciya = PreferenceManager.getDefaultSharedPreferences(this).getString("Integraciya", "")
        val clent_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Cleand_id", "")
        val child_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Child_id", "")
        val email = PreferenceManager.getDefaultSharedPreferences(this).getString("email", "")
        val password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "")
        val PHPSESSID = PreferenceManager.getDefaultSharedPreferences(this).getString("PHPSESSID", "")
        webView = WebView(this)
        setContentView(webView)
        webView.visibility = View.GONE
        val url = "https://aspiot.ru/client_login?ajax=1"

        val postData = "login=${URLEncoder.encode("$email", "UTF-8")}" +
                "&password=${URLEncoder.encode("$password", "UTF-8")}"
        webView.postUrl(url, postData.toByteArray())

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful){
                return@OnCompleteListener
            }
            token = task.result!!
            Log.d("token", task.result!!)
        })

        url_ = intent.getStringExtra("url").toString()

        val sub = url_!!.substringBefore("tps:")

        if(sub == "ht"){
            argument = 1
        }

        Handler().postDelayed(
            {
                callServer("https://aspiot.ru/webhooks/push.php?PHPSESSID=$PHPSESSID&token=$token")
                argument = 0
            }, 1000
        )

        if(argument == 0) {


            webView!!.postUrl(url, postData.toByteArray())
            Handler().postDelayed(
                {
                    if (success == "true") {
                        webView.loadUrl("https://aspiot.ru/market?client_id=$clent_id&child_id=$child_id")

                    }
                }, 2000
            )

            Handler().postDelayed(
                {
                    if (success == "true") {
                        webView.visibility = View.VISIBLE
                    }
                },3000)
        }else{
            webView!!.postUrl(url, postData.toByteArray())
            argument = 0
            Handler().postDelayed(
                {
                    webView.loadUrl(url_!!)
                },2000)

            Handler().postDelayed(
                {
                        webView.visibility = View.VISIBLE
                },3000)
        }

//        Firebase.messaging.isAutoInitEnabled = true
        webView.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(
                view: WebView?, handler: SslErrorHandler,
                error: SslError
            ) {
                val builder = AlertDialog.Builder(this@Webview)
                var message = "SSL Certificate error."
                when (error.primaryError) {
                    SslError.SSL_UNTRUSTED -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "SslError : The certificate authority is not trusted."
                        )
                        message = "The certificate authority is not trusted.";
                    }
                    SslError.SSL_EXPIRED -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "SslError : The certificate has expired."
                        )
                        message = "The certificate has expired.";
                    }
                    SslError.SSL_IDMISMATCH -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate Hostname mismatch."
                        )
                        message = "The certificate Hostname mismatch."
                    }
                    SslError.SSL_NOTYETVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate is not yet valid."
                        )
                        message = "The certificate is not yet valid."
                    }
                    SslError.SSL_DATE_INVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate date is not valid."
                        )
                        message = "The certificate date is not valid."
                    }
                    SslError.SSL_INVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate is not valid."
                        )
                        message = "The certificate is not valid."
                    }
                }
                message += " Do you want to continue anyway?"

                builder.setTitle("SSL Certificate Error")
                builder.setMessage(message)

                builder.setPositiveButton(
                    "continue"
                ) { dialog, which -> handler.proceed() }
                builder.setNegativeButton(
                    "cancel"
                ) { dialog, which -> handler.cancel() }
                val dialog = builder.create()
                dialog.show()
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.e("Success", "URL $url")

                firstUrl = PreferenceManager.getDefaultSharedPreferences(this@Webview)
                    .getString("URL_FIRST", "")!!
                Log.e("Success", "URL First $firstUrl")

                if (url!!.contains(firstUrl)) {
                    if (isFirst)
                        PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
                            .putString("URL_FIRST", url!!.substringAfter("://")).apply()
                    if (url.contains("http")) {
                        Log.e("LOG_TAG", "shouldOverrideUrlLoading URL1: $url ")
                        view?.loadUrl(url)
                        isFirst = false
                    }
                } else {
                    val list = urlData!!.split(",")
                    var isTrue = false
                    for (item in list) {
                        if (url.contains(item)) {
                            isTrue = true
                            isDomain = true
                        }
                    }
                    if (!isDomain || isTrue) {
                        Log.e("LOG_TAG", "shouldOverrideUrlLoading URL1: $url ")
                        view?.loadUrl(url)

                    } else {
                        try {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            intent.data = Uri.parse(url)
                            startActivity(intent)
                        } catch (e: Exception) {
                            Log.e("TAG", "Error no such program")

                        }
                    }


                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)
            }
        }
        configureWebViewSettings()
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {

                Witi(url)

                if (!url.contains("http")) {
                    val intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK
                    )
                    startActivity(intent)
                    return true
                } else {
                    webView.loadUrl(url)
                    return true
                }
                return false
            }
        }



        webView.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            //filename of downloading file
            var filename = URLUtil.guessFileName(url, contentDisposition, mimetype)

            //alertdialog builder created
            val builder = AlertDialog.Builder(this)
            //alertdialog title set
            builder.setTitle("Download")
            //alertdialog message set
            builder.setMessage("Do you want to save $filename")
            //if yes clicks,following code will executed
            builder.setPositiveButton("Yes") { dialog, which ->
                //DownloadManager request created based on url
                val request = DownloadManager.Request(Uri.parse(url))
                //get cookie
                val cookie = CookieManager.getInstance().getCookie(url)
                //add cookie to request
                request.addRequestHeader("Cookie", cookie)
                //add User-agent to request
                request.addRequestHeader("User-Agent", userAgent)
                //Files are scanned before downloading
                request.allowScanningByMediaScanner()
                //download notification is visible while downloading and after download completion
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                //DownloadManager Service created
                val downloadmanager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                //Files are downloaded to Download folder
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
                //download starts
                downloadmanager.enqueue(request)
            }
            builder.setNegativeButton("Cancel")
            { dialog, which ->
                //dialog cancels
                dialog.cancel()
            }
            //alertdialog created
            val dialog: AlertDialog = builder.create()
            //shows alertdialog
            dialog.show()
        }
    }

    fun Witi(url:String){
        val url_save = url
        if(url_save == "https://aspiot.ru/?logout" || url_save == "https://aspiot.ru/client_login?ajax=1" || url_save == "https://aspiot.ru/client_login"){
            val intent = Intent(this@Webview, MainActivity::class.java)
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
                .putInt("true_false", 1).apply()
            startActivity(intent)
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
                .putBoolean(
                    IS_FIRST, false
                ).apply()
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@Webview).edit()
                .putBoolean(
                    IS_SECOND, false
                ).apply()
        }
    }

//    private fun checkInternetConnection() = ConnectionService.isConnected()

//    override fun onResume() {
//        super.onResume()
//        val intFilter = IntentFilter()
//        intFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
//
//        receiver = ConnectionService()
//        registerReceiver(receiver, intFilter)
//    }

    private fun configureWebViewSettings() {
        webView.settings.javaScriptEnabled = true
        webView.settings.setAppCacheEnabled(true)
        webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = false
        webView.settings.blockNetworkImage = false
        webView.settings.loadsImagesAutomatically = true
        webView.settings.useWideViewPort = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.allowFileAccess = true
        webView.settings.domStorageEnabled = true
        webView.settings.setSupportMultipleWindows(true)
        webView.settings.loadWithOverviewMode = true
        webView.settings.allowContentAccess = true
        webView.settings.setGeolocationEnabled(true)
        webView.settings
        webView.webChromeClient = getCustomWebChromeClient()
    }

    private fun getCustomWebChromeClient() = object : WebChromeClient() {

        override fun onCreateWindow(
            view: WebView?,
            isDialog: Boolean,
            isUserGesture: Boolean,
            resultMsg: Message?
        ): Boolean {
            val result = view!!.hitTestResult
            val data = result.extra
            view.loadUrl(data!!)
            return false
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onPermissionRequest(request: PermissionRequest) {
            request.grant(request.resources)
        }
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onShowFileChooser(
            view: WebView?,
            filePath: ValueCallback<Array<Uri>>?,
            fileChooserParams: FileChooserParams?
        ): Boolean {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
            mUploadMessage?.onReceiveValue(null)
            mUploadMessage = null
            mUploadMessage = filePath

            val takePictureIntent = createImageCaptureIntent()

            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = INTENT_FILE_TYPE

            val intentArray: Array<Intent?>
            intentArray = arrayOf(takePictureIntent)

            val chooserIntent = Intent(Intent.ACTION_CHOOSER)
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            chooserIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.file_chooser_title))
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)

            try {
                startActivityForResult(chooserIntent, REQUEST_SELECT_FILE)
            } catch (e: ActivityNotFoundException) {
                mUploadMessage = null
                cameraImagePath = null

                Toast.makeText(
                    this@Webview,
                    getString(R.string.cannot_open_file_chooser_txt),
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            return true
        }
    }


    private fun createImageCaptureIntent(): Intent? {
        var captureImageIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (captureImageIntent?.resolveActivity(packageManager) != null) {
            var imageFile: File? = null

            try {
                imageFile = createImageFile()
                captureImageIntent.putExtra("CameraImagePath", cameraImagePath)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

            if (imageFile != null) {
                cameraImagePath = CAMERA_PHOTO_PATH_POSTFIX + imageFile.absolutePath
                captureImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile))
            } else {
                captureImageIntent = null
            }
        }

        return captureImageIntent
    }

    private fun createImageFile(): File? {
        val timeStamp = SimpleDateFormat.getDateInstance().format(Date())
        val imageFileName = PHOTO_NAME_POSTFIX + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, PHOTO_FORMAT, storageDir)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != REQUEST_SELECT_FILE || mUploadMessage == null) return

        var results: Array<Uri>? = null

        val resultargument = resultCode
        val resultok = RESULT_OK
        if (resultCode == RESULT_OK) {
            if (data != null) {
                if (cameraImagePath != null) results = arrayOf(Uri.parse(cameraImagePath))
            } else {
                val dataString = data?.dataString
                if (dataString != null) results = arrayOf(Uri.parse(dataString))
            }
        }
        mUploadMessage?.onReceiveValue(results)
        mUploadMessage = null
    }

    override fun onStop() {
        super.onStop()
        setURL(webView.url)
    }

    private fun setURL(url: String?) {
        val editor = PreferenceManager.getDefaultSharedPreferences(this).edit()
        editor.putString(SHARED_PREFS_URL, url)
        editor.apply()
    }

    private fun unHideToLog(str: String): String {
        var str = str
        str = str.replace("-", "")
        var result = ""
        var i = 0
        while (i < str.length) {
            val hex = str.substring(i + 1, i + 3)
            result += (hex.toInt(16) xor str[i].toString().toInt()).toChar()
            i += 3
        }
        Log.i("UNHIDE", "OK $result")
        result_url = result
        return  result
    }

    private fun doBack(): Boolean {
        if (webView.canGoBack()) {
            webView.goBack()
            return true
        }
        return false
    }

    override fun onBackPressed() {
        val suburl = webView.url.toString()
        val url = suburl.substringBefore("_id")
        if(url == "https://aspiot.ru/market/client?client"){
            doBack()
        }else{

        }
    }


    private fun callServer(serve: String){
        val httpAsync = serve
            .httpGet()
            .timeout(20000)
            .header("Content-Type", "application/json; utf-8")
            .responseString { _, _, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        Log.e("Error", "response error ${result.getException()}")
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                    is Result.Success -> {
                        val data = result.get().toString()
                        println(data)

                        val integraciya = PreferenceManager.getDefaultSharedPreferences(this).getString("Integraciya", "")
                        val clent_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Cleand_id", "")
                        val child_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Child_id", "")
                        val sub_success = data.substringAfter("\"success\":")
                        success = sub_success.substringBefore(',')

                    }
                }
            }
        httpAsync.join()
    }

//    override fun onNetworkConnection(isConnected: Boolean) {
//        if (!isConnected) {
//            val builder = AlertDialog.Builder(this)
//            builder.setMessage("Internet is not connected, please connect to internet")
//                .setTitle("Connection Error")
//                .setCancelable(false).setPositiveButton("Try Again") { dialog, numb ->
//                    if (checkInternetConnection()) {
//                        dialog.dismiss()
//                        webView.reload()
//                    }
//                    else
//                        builder.show()
//                }
//            builder.show()
//        }
//    }

    override fun onDestroy() {
        super.onDestroy()
    }
}