package com.app.dmitrap

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.icu.number.NumberFormatter.with
import android.icu.number.NumberRangeFormatter.with
import android.media.RingtoneManager
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.bumptech.glide.GenericTransitionOptions.with
import com.bumptech.glide.Glide.with
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.with
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.with
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import me.leolin.shortcutbadger.ShortcutBadger


const val ARGUMENT = "ARGUMENT"
class MyFirebaseMessagingService : FirebaseMessagingService() {

    var argumenti = 0
    var argumenti_intent = 1

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)


        val data = remoteMessage.data.toString()
        val isargument =
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this).getBoolean(ARGUMENT, false)
        if(isargument){
            argumenti++
        }
        val alert = remoteMessage.data.toString()
        val subalert_ = alert.substringAfter("al")
        val alert_ = subalert_.substringBefore("rt")
        if(alert_ == "e"){
            val subtitle = data.substringAfter("\"title\":\"")
            val title = subtitle.substringBefore('"')
            val subbody = data.substringAfter("\"body\":\"")
            val body = subbody.substringBefore('"')

            val suburl = data.substringAfter("\"url\":\"")
            val url = suburl.substringBefore('"')
//        val title = remoteMessage.notification!!.title
//        val body = remoteMessage.notification!!.body
            sendNotification("$body","$title","$url")
            val badgeCount = 1
        ShortcutBadger.applyCount(this, badgeCount) //for 1.1.4+

        }else{
            val esns = ""
        }
////        val body = remoteMessage.data.getValue("body")
////        val url = remoteMessage.data.getValue("url")
////        val title = alert.getAsJsonObject("title")
//        val subtitle = data.substringAfter("\"title\":\"")
//        val title = subtitle.substringBefore('"')
//        val subbody = data.substringAfter("\"body\":\"")
//        val body = subbody.substringBefore('"')
//
//        val suburl = data.substringAfter("\"url\":\"")
//        val url = suburl.substringBefore('"')
////        val title = remoteMessage.notification!!.title
////        val body = remoteMessage.notification!!.body
//        sendNotification("$body","$title","$url")
//        ReceiverCallNotAllowedException(remoteMessage.toString())
        Log.d("Tag", "info = ${remoteMessage.data}        $alert")


    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
    }




    @RequiresApi(Build.VERSION_CODES.O)
    private fun sendNotification(messageBody: String, messagetitle:String, url:String) {

        val argument = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(IS_NOTIVICATION, false)
        if(argument) {
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(
                    ARGUMENT, true
                ).apply()
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putInt("argument", argumenti).apply()
            val arguments_ = PreferenceManager.getDefaultSharedPreferences(this).getInt("argument", 0)
            val arguments = 1 + arguments_

            var notificationBuilder:NotificationCompat.Builder? = null
                val intent = Intent(this, Webview::class.java)
                if (url != null) {
                    intent.putExtra("url", url)
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val pendingIntent = PendingIntent.getActivity(
                    this, arguments /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT
                )
                val channelId = getString(R.string.default_notification_channel_id)
                val defaultSoundUri =
                    RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                notificationBuilder = NotificationCompat.Builder(this, arguments.toString())
                    .setSmallIcon(R.drawable.a)
                    .setContentTitle(messagetitle)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)

//                .setStyle(NotificationCompat.InboxStyle()
//                    .addLine(" Olicia : Hey!,what's the progress of the project")
//                    .addLine("James : Bud,Check This Out ")
//                    .setBigContentTitle("2 new messages")
//                    .setSummaryText("Friends Group"))
//                    .setPriority(NotificationCompat.PRIORITY_LOW)
//                    .setGroup("example_group")
//                    .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_CHILDREN)
//                    .setGroupSummary(true)
                argumenti_intent = 2
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    arguments.toString(),
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                channel.enableVibration(true)
                channel.enableLights(true)
                channel.setShowBadge(true)
                notificationManager.createNotificationChannel(channel)
            }


//            val id = "my_channel_01"
//            val name = getString(R.string.channel_name)
//            val descriptionText = getString(R.string.channel_description)
//            val importance = NotificationManager.IMPORTANCE_LOW


//            val target: View = findViewById(R.id.target_view)
//            val badge = BadgeView(this, target)
//            badge.setText("1")
//            badge.show()


            notificationManager.notify(arguments /* ID of notification */, notificationBuilder.build())

        }
    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }
}