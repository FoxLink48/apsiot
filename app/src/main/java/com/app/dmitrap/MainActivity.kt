package com.app.dmitrap

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.os.*
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.preference.PreferenceManager
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.github.arturogutierrez.Badges
import com.github.arturogutierrez.BadgesNotSupportedException
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging


const val IS_FIRST = "IS_FIRST"
const val IS_SECOND = "IS_SECOND"
const val IS_SECOND2 = "IS_SECOND2"
const val IS_NOTIVICATION = "IS_NOTIVICATION"
class MainActivity : AppCompatActivity() {

    private var cancellationSignal: CancellationSignal? = null

    private val authenticationCallback: BiometricPrompt.AuthenticationCallback
        get() =
            @RequiresApi(Build.VERSION_CODES.P)
            object : BiometricPrompt.AuthenticationCallback(){
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                    super.onAuthenticationError(errorCode, errString)
                    notifyUser("Authentication error:$errString")
                    if(errString == "No fingerprints enrolled."){
                        startActivity(Intent(this@MainActivity,Webview::class.java))
                    }
                }
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                    super.onAuthenticationSucceeded(result)
                    notifyUser("Authentication success")
                    PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                        .putInt("true_false", 0).apply()
                    startActivity(Intent(this@MainActivity, Webview::class.java))
                }
            }

    lateinit var button: Button
    lateinit var zabil: TextView
    lateinit var registr: TextView
    lateinit var textView19: TextView
    lateinit var editTextTextPersonName: EditText
    lateinit var editTextTextPersonName2: EditText
    var mRequestQueue: RequestQueue? = null
    var argument = 0

    var true_false = 1
    @SuppressLint("StringFormatInvalid")
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = intent.getIntExtra("key",1)

        true_false = intent
        window.decorView.apply {
            // Hide both the navigation bar and the status bar.
            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
            // a general rule, you should design your app to hide the status bar whenever you
            // hide the navigation bar.

            systemUiVisibility =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        true_false =
            PreferenceManager.getDefaultSharedPreferences(this).getInt("true_false", 1)

        if(true_false == 1) {
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(
                    IS_NOTIVICATION, false
                ).apply()

            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(
                    IS_URL, false
                ).apply()

            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(
                    IS_FIRST, false
                ).apply()

            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(
                    IS_SECOND, false
                ).apply()

            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(
                    IS_SECOND2, false
                ).apply()
        }


        val isUrl =
            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(IS_URL, false)
        val isFirstTime =
            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(IS_FIRST, false)
        val secondTime =
            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(IS_SECOND, false)
        val secondTime2 =
            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(IS_SECOND2, false)
        val isNotification =
            PreferenceManager.getDefaultSharedPreferences(this).getBoolean(IS_NOTIVICATION, false)
        if (!isFirstTime) {
            Handler(Looper.myLooper()!!).postDelayed(
                {
                    isUrl
                    click()
                }, 500)
        } else {
            if(secondTime2){
                startActivity(Intent(this,ZabilActivity::class.java))
                PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                    .putInt("argumentzabil", 0).apply()
                PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                    .putInt("true_false", 0).apply()
            }else {
                if (!secondTime) {
                    startActivity(Intent(this, RegActivity::class.java))
                    PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                        .putInt("argumentregistr", 0).apply()
                    PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                        .putInt("true_false", 0).apply()
                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                        .putBoolean(
                            IS_NOTIVICATION, true
                        ).apply()
                    finish()
                } else {
                    startActivity(Intent(this, Webview::class.java))
                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                        .putBoolean(
                            IS_NOTIVICATION, true
                        ).apply()
                    PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                        .putInt("true_false", 0).apply()
                    finish()
                }
            }

        }

        mRequestQueue = Volley.newRequestQueue(this)

        button = findViewById(R.id.button)
        zabil = findViewById(R.id.zabil)
        registr = findViewById(R.id.registr)
        editTextTextPersonName = findViewById(R.id.editTextTextPersonName)
        editTextTextPersonName2 = findViewById(R.id.editTextTextPersonName2)
        textView19 = findViewById(R.id.textView19)
    }


    @RequiresApi(Build.VERSION_CODES.P)
    fun click(){
        button.setOnClickListener {

            val email = editTextTextPersonName.text
            val password = editTextTextPersonName2.text

            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString("email", email.toString()).apply()
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString("password", password.toString()).apply()
            if(email.isNotEmpty() && password.isNotEmpty()) {
                textView19.visibility = View.INVISIBLE
                val params = listOf("login" to "$email", "password" to "$password")
                Fuel.upload("https://aspiot.ru/client_login?ajax=1", Method.POST, params)
                    .responseString { _, _, result ->
                        when (result) {
                            is Result.Failure -> {
                                print(result.getException().toString())
                                argument = 1
                                error(argument)
                            }
                            is Result.Success -> {
                                val data = result.get()
                                print(data)
                                val subdata = data.substringAfter("\"redirect\"")
                                val subdata_ = subdata.substringBefore('"')
                                val subintegraciya = data.substringAfter("\"redirect\":\"\\")
                                val integraciya = subintegraciya.substringBefore('"')
                                val sub_clent_id = data.substringAfter("\"client_id\":")
                                val cleant_id = sub_clent_id.substringBefore(',')
                                val sub_child_id = data.substringAfter("\"child_id\":")
                                val child_id = sub_child_id.substringBefore(',')
                                val subPHPSESSID = data.substringAfter("\"PHPSESSID\":\"")
                                val PHPSESSID = subPHPSESSID.substringBefore('"')
                                if (subdata_ == ":") {
//                                    startActivity(Intent(this,Webview::class.java))
                                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                                        .putString("PHPSESSID", PHPSESSID).apply()
                                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                                        .putString("Integraciya", integraciya).apply()
                                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                                        .putString("Cleand_id", cleant_id).apply()
                                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                                        .putString("Child_id", child_id).apply()
                                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                                        .putBoolean(
                                            IS_NOTIVICATION, true
                                        ).apply()
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        val fingerprintManager = FingerprintManagerCompat.from(this)
                                        if (!fingerprintManager .isHardwareDetected) {
                                            PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                                                .putInt("true_false", 0).apply()
                                            startActivity(Intent(this,Webview::class.java))
                                            Log.e("tag","not detected.")
                                        } else if (!fingerprintManager .hasEnrolledFingerprints()) {
                                            Log.e("tag","No fingerprint is set")
                                            PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                                                .putInt("true_false", 0).apply()
                                            startActivity(Intent(this,Webview::class.java))
                                        } else {
                                            biometric()
                                            Log.e("tag","Fingerprint is set")
                                        }
                                    }

                                    argument = 0
                                    error(argument)
                                } else {
                                    argument = 1
                                    error(argument)
                                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                                        .putBoolean(
                                            IS_FIRST, false
                                        ).apply()
                                }
                            }
                        }
                    }
            }else {
                textView19.visibility = View.VISIBLE
                textView19.text = "Логин или пароль эти строки не может быть пустым"
            }
//            callServer(
//                "https://aspiot.ru/client_login?ajax=1",
//                email.toString(),
//                password.toString()
//            )
//            Handler().postDelayed(
//                {
//                    if(argument == 1){
//                        textView19.visibility = View.VISIBLE
//                        textView19.text = "пользователь не найден"
//                    }else{
//                        textView19.visibility = View.INVISIBLE
//                    }
//                },2000

//            )
        }


        zabil.setOnClickListener {
            val intent = Intent(this, ZabilActivity::class.java)
            PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                .putInt("argumentzabil", 1).apply()
            startActivity(intent)
        }

        registr.setOnClickListener {
            val intent = Intent(this, RegActivity::class.java)
            PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit()
                .putInt("argumentregistr", 1).apply()
            startActivity(intent)
        }
    }


    @RequiresApi(Build.VERSION_CODES.P)
    fun biometric (){
        val biometricPrompt = BiometricPrompt.Builder(this)
            .setTitle("Title of prompt")
            .setSubtitle("Authentication is required")
            .setDescription("This app uses fingerprint protectionto keep your data secure")
            .setNegativeButton("Cancle", this.mainExecutor,
                DialogInterface.OnClickListener { dialog, which ->
                    notifyUser("Authentication cancelled")
                }).build()

        biometricPrompt.authenticate(getCancellationSignal(),mainExecutor,authenticationCallback)
    }

    fun error(argument_ :Int){
        if(argument_ == 1){
            textView19.visibility = View.VISIBLE
            textView19.text = "пользователь не найден"
        }else{
            textView19.visibility = View.INVISIBLE
        }
    }

    private fun notifyUser(message:String){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }

    private fun getCancellationSignal(): CancellationSignal {
        cancellationSignal = CancellationSignal()
        cancellationSignal?.setOnCancelListener {
            notifyUser("Authentication was cancelled by the user")
        }
        return  cancellationSignal as CancellationSignal
    }
    private fun checkBiometricSupport():Boolean{
        val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        if(!keyguardManager.isKeyguardSecure){
            notifyUser("Fingerprint authentication has not been enabled in settings")
            return false
        }

        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.USE_BIOMETRIC) != PackageManager.PERMISSION_GRANTED){
            notifyUser("Fingerprint authentication permission is not enabled")
            return false
        }
        return if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)){
            true
        }else true
    }

    private fun callServer(serve: String, email: String, password: String) {
        val bodyJson = """{ "title" : "$email", "body" : "login", "id" : "1" }"""
//        val bodyJson2 = """{ "title" : "$password", "body" : "password", "id" : "2" }"""
//        val bodyJson3 = """{ "title" : "foo","body" : "bar","id" : "1"}"""
        val httpAsync = serve
            .httpPost()
            .timeout(20000)
            .body(bodyJson)
//            .body(bodyJson2)
            .responseString { _, _, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        Log.e("Error", "response error ${result.getException()}")
                    }
                    is Result.Success -> {
                        val data = result.get().toString()
                        println(data)
                        Log.e("Succses", "response success ${data}")
                        PreferenceManager.getDefaultSharedPreferences(this).edit()
                            .putBoolean(
                                IS_SECOND, true
                            ).apply()

                        val subdata = data.substringAfter("\"redirect\"")
                        val subdata_ = subdata.substringBefore('"')
                        if (subdata_ == ":") {
                            startActivity(Intent(this, WebActivity::class.java))
                        }
                    }
                }
            }
        httpAsync.join()
    }


    override fun onDestroy() {
        super.onDestroy()
    }
    override fun onBackPressed() {
    }
}
