package com.app.dmitrap

import android.app.AlertDialog
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Uri
import android.net.http.SslError
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.webkit.*
import java.net.URLEncoder

class WebActivity : AppCompatActivity() {


    companion object {
        private const val CAMERA_REQUEST_CODE = 113
        private const val REQUEST_SELECT_FILE = 1
        private const val INTENT_FILE_TYPE = "image/*"
        private const val CAMERA_PHOTO_PATH_POSTFIX = "file:"
        private const val PHOTO_NAME_POSTFIX = "JPEG_"
        private const val PHOTO_FORMAT = ".jpg"
        var firstUrl = ""
        var isFirst = true
        var isDomain = false
        var result_url:String? = null
    }


    var webView:WebView? = null
    var editor:SharedPreferences.Editor? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        window.decorView.apply {
            // Hide both the navigation bar and the status bar.
            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
            // a general rule, you should design your app to hide the status bar whenever you
            // hide the navigation bar.

            systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        val integraciya = PreferenceManager.getDefaultSharedPreferences(this).getString("Integraciya", "")
        val clent_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Cleand_id", "")
        val child_id = PreferenceManager.getDefaultSharedPreferences(this).getString("Child_id", "")
        val email = PreferenceManager.getDefaultSharedPreferences(this).getString("email", "")
        val password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "")
        webView = WebView(this)
        setContentView(webView)
        webView!!.visibility = View.GONE
        webView!!.setWebViewClient(WebViewClient())
        val url = "https://aspiot.ru/client_login?ajax=1"

        val postData = "login=${URLEncoder.encode("$email", "UTF-8")}" +
                "&password=${URLEncoder.encode("$password", "UTF-8")}"
        webView!!.postUrl(url, postData.toByteArray())

        Handler().postDelayed(
            {
                webView!!.setWebViewClient(WebViewClient())
                webView!!.loadUrl("www.google.com")

            },1000
        )
 //       https://aspiot.ru$integraciya?client_id=$clent_id&child_id=$child_id
        Handler().postDelayed(
            {
        webView!!.visibility = View.VISIBLE
    },3000
    )

        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.setAppCacheEnabled(true)
        webView!!.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView!!.settings.setSupportZoom(true)
        webView!!.settings.builtInZoomControls = false
        webView!!.settings.blockNetworkImage = false
        webView!!.settings.loadsImagesAutomatically = true
        webView!!.settings.useWideViewPort = true
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webView!!.settings.allowFileAccess = true
        webView!!.settings.domStorageEnabled = true
        webView!!.settings.setSupportMultipleWindows(true)
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.allowContentAccess = true
        webView!!.settings.setGeolocationEnabled(true)

        val urlData = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .getString("URL_2_DATA", "")

        webView!!.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(
                view: WebView?, handler: SslErrorHandler,
                error: SslError
            ) {
                val builder = AlertDialog.Builder(this@WebActivity)
                var message = "SSL Certificate error."
                when (error.primaryError) {
                    SslError.SSL_UNTRUSTED -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "SslError : The certificate authority is not trusted."
                        )
                        message = "The certificate authority is not trusted.";
                    }
                    SslError.SSL_EXPIRED -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "SslError : The certificate has expired."
                        )
                        message = "The certificate has expired.";
                    }
                    SslError.SSL_IDMISMATCH -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate Hostname mismatch."
                        )
                        message = "The certificate Hostname mismatch."
                    }
                    SslError.SSL_NOTYETVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate is not yet valid."
                        )
                        message = "The certificate is not yet valid."
                    }
                    SslError.SSL_DATE_INVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate date is not valid."
                        )
                        message = "The certificate date is not valid."
                    }
                    SslError.SSL_INVALID -> {
                        Log.d(
                            "TEST_OF_ORDER_LINK",
                            "The certificate is not valid."
                        )
                        message = "The certificate is not valid."
                    }
                }
                message += " Do you want to continue anyway?"

                builder.setTitle("SSL Certificate Error")
                builder.setMessage(message)

                builder.setPositiveButton(
                    "continue"
                ) { dialog, which -> handler.proceed() }
                builder.setNegativeButton(
                    "cancel"
                ) { dialog, which -> handler.cancel() }
                val dialog = builder.create()
                dialog.show()
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.e("Success", "URL $url")

                firstUrl = PreferenceManager.getDefaultSharedPreferences(this@WebActivity)
                    .getString("URL_FIRST", "")!!
                Log.e("Success", "URL First $firstUrl")

                if (url!!.contains(firstUrl)) {
                    if (isFirst)
                        PreferenceManager.getDefaultSharedPreferences(this@WebActivity).edit()
                            .putString("URL_FIRST", url!!.substringAfter("://")).apply()
                    if (url.contains("http")) {
                        Log.e("LOG_TAG", "shouldOverrideUrlLoading URL1: $url ")
                        view?.loadUrl(url)
                        isFirst = false
                    }
                } else {
                    val list = urlData!!.split(",")
                    var isTrue = false
                    for (item in list) {
                        if (url.contains(item)) {
                            isTrue = true
                            isDomain = true
                        }
                    }
                    if (!isDomain || isTrue) {
                        Log.e("LOG_TAG", "shouldOverrideUrlLoading URL1: $url ")
                        view?.loadUrl(url)

                    } else {
                        try {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            intent.data = Uri.parse(url)
                            startActivity(intent)
                        } catch (e: Exception) {
                            Log.e("TAG", "Error no such program")

                        }
                    }


                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)
            }
        }

        webView!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {

                Witi()

                if (!url.contains("https")) {
                    val intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK
                    )
                    startActivity(intent)
                    return true
                } else {
                    webView.loadUrl(url)
                    return true
                }
                return false
            }
        }




        webView!!.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            //filename of downloading file
            var filename = URLUtil.guessFileName(url, contentDisposition, mimetype)

            //alertdialog builder created
            val builder = AlertDialog.Builder(this)
            //alertdialog title set
            builder.setTitle("Download")
            //alertdialog message set
            builder.setMessage("Do you want to save $filename")
            //if yes clicks,following code will executed
            builder.setPositiveButton("Yes") { dialog, which ->
                //DownloadManager request created based on url
                val request = DownloadManager.Request(Uri.parse(url))
                //get cookie
                val cookie = CookieManager.getInstance().getCookie(url)
                //add cookie to request
                request.addRequestHeader("Cookie", cookie)
                //add User-agent to request
                request.addRequestHeader("User-Agent", userAgent)
                //Files are scanned before downloading
                request.allowScanningByMediaScanner()
                //download notification is visible while downloading and after download completion
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                //DownloadManager Service created
                val downloadmanager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                //Files are downloaded to Download folder
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
                //download starts
                downloadmanager.enqueue(request)
            }
            builder.setNegativeButton("Cancel")
            { dialog, which ->
                //dialog cancels
                dialog.cancel()
            }
            //alertdialog created
            val dialog: AlertDialog = builder.create()
            //shows alertdialog
            dialog.show()
        }


    }

    fun Witi(){
        val url_save = webView!!.url
        if(url_save == "https://aspiot.ru/?logout" || url_save == "https://aspiot.ru/client_login?ajax=1"){
            startActivity(Intent(this@WebActivity,MainActivity::class.java))
        }

        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@WebActivity).edit()
            .putBoolean(
                IS_FIRST, false
            ).apply()
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this@WebActivity).edit()
            .putBoolean(
                IS_SECOND, false
            ).apply()
    }

    override fun onStop() {
        super.onStop()
        setURL(webView!!.url)
    }


    private fun setURL(url: String?) {
        editor = PreferenceManager.getDefaultSharedPreferences(this).edit()
        editor!!.putString(SHARED_PREFS_URL, url)
        editor!!.apply()
    }

    private fun doBack(): Boolean {
        if (webView!!.canGoBack()) {
            webView!!.goBack()
            return true
        }
        return false
    }

    override fun onBackPressed() {
        doBack()
    }

}